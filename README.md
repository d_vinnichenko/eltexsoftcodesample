# EltexSoft Code Example

### This code displays an example of the implementation of some elements and parts of the application.

*- Literacy and Simplicity -*

Our team believes that the application code should combine literacy and ease of writing, even of some of the most complex things.
That's why we use such a [wonderful combination](https://gitlab.com/d_vinnichenko/swift-style-guidelines) as **CleanSwift** architecture + **SwiftLint**, which allows you to set a lot of rules for writing code.
