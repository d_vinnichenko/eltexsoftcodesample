//
//  NetworkService + Authorization.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 5/17/19.
//  Copyright © 2019 Petryk Dima. All rights reserved.
//

import Alamofire

extension NetworkService {
    
    func login(name: String, password: String, responseSuccess: @escaping(_ token: String) -> Void, responseError: @escaping(CustomError?) -> Void) {
        
        let parameters = ["username": name, "password": password] as [String: String]
        
        let request = Alamofire.request(
            APPURL.authorization,
            method: .post,
            parameters: parameters,
            encoding: JSONEncoding.default,
            headers: nil
        )
        
        request.responseJSON { response in
            switch response.result {
                
            case .success(let value):
                let (error, _, _) = self.checkError(from: response)
                
                // Check response objects
                if let error = error {
                    if error.statusCode != 666 {
                        responseError(error)
                        return
                    }
                }
                
                if let valueResponse = value as? [String: String] {
                    let token = valueResponse["token"] ?? ""
                    
                    UserDataService.sharedObject.saveAuthToken(authToken: token)
                    
                    responseSuccess(token)
                } else {
                    responseError(error)
                }
                
            case .failure:
                let customError = CustomError(statusCode: response.response?.statusCode, message: "Something went wrong")
                responseError(customError)
            }
        }
    }
}
