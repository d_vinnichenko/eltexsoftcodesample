//
//  NetworkService + Adress.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 5/16/19.
//  Copyright © 2019 Petryk Dima. All rights reserved.
//

import Alamofire

extension NetworkService {
    
    func createAddress(street: String, status: Bool, responseSuccess: @escaping() -> Void, responseError: @escaping(CustomError?) -> Void) {
        
        let parameters = [
            "street": street,
            "status": status ? "true": "false"
        ] as [String: String]
        
        let request = Alamofire.request(
            APPURL.adressList,
            method: .post,
            parameters: parameters,
            encoding: JSONEncoding.default,
            headers: loadHeaders()
        )
        
        request.responseJSON { response in
            switch response.result {
                
            case .success:
                let (error, _, _) = self.checkError(from: response)
                
                // Check response objects
                if let error = error {
                    if error.statusCode != 666 {
                        responseError(error)
                        return
                    }
                }
                
                responseSuccess()
                return
                
            case .failure:
                let customError = CustomError(statusCode: response.response?.statusCode, message: "Something went wrong")
                responseError(customError)
            }
        }
    }
    
    func getAdressList(searchWord: String?, responseSuccess: @escaping([Adress]) -> Void, responseError: @escaping(CustomError?) -> Void) {
        
        let parameters = ["street_filter": searchWord ?? ""] as [String: String]
        
        let request = Alamofire.request(
            APPURL.adressList,
            method: .get,
            parameters: parameters,
            encoding: URLEncoding.queryString,
            headers: loadHeaders()
        )
        
        request.responseJSON { response in
            switch response.result {
                
            case .success(let value):
                let (error, _, _) = self.checkError(from: response)
                var content: [Adress] = []
                
                // Check response objects
                if let error = error {
                    if error.statusCode != 666 {
                        responseError(error)
                        return
                    }
                }
                
                if let arrayData = value as? [[String: Any]] {
                    
                    for adressDic in arrayData {
                        if let adress = self.convertData(dataDict: adressDic, to: Adress.self) {
                            content.append(adress)
                        }
                    }
                    
                    // Send success
                    responseSuccess(content)
                    return
                }
                
            case .failure:
                let customError = CustomError(statusCode: response.response?.statusCode, message: "Something went wrong")
                responseError(customError)
            }
        }
    }
    
    func updateAddress(street: String, status: Bool, id: Int, responseSuccess: @escaping() -> Void, responseError: @escaping(CustomError?) -> Void) {
        
        let parameters = [
            "street": street,
            "status": status
        ] as [String: Any]
        
        let request = Alamofire.request(
            APPURL.adressList + String("\(id)/"),
            method: .put,
            parameters: parameters,
            encoding: JSONEncoding.default,
            headers: loadHeaders()
        )
        
        request.responseJSON { response in
            
            switch response.result {
                
            case .success:
                let (error, _, _) = self.checkError(from: response)
                
                // Check response objects
                if let error = error {
                    if error.statusCode != 666 {
                        responseError(error)
                        return
                    }
                }
                
                responseSuccess()
                
            case .failure:
                let customError = CustomError(statusCode: response.response?.statusCode, message: "Something went wrong")
                responseError(customError)
            }
        }
    }
}

