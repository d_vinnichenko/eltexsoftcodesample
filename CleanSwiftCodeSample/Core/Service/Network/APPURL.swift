//
//  APPURL.swift
//  FSF
//
//  Created by Petryk Dima on 4/24/19.
//  Copyright © 2019 Petryk Dima. All rights reserved.
//

import Foundation

enum APPURL {
    
    enum Domains {
        static let MainDomain = "https://staging.freestufffinder.com"
    }
    
    enum Routes {
        static let Api = "/wp-json/wp/v2/"
    }
    
    static let Domain = Domains.MainDomain
    static let Route = Routes.Api
    static let BaseURL = "http://165.22.137.39:15555/"
    
    static var authorization: String { return BaseURL + "api-token-auth/" }
    
    static var contactList: String { return BaseURL + "contact/" }
    static var adressList: String { return BaseURL + "address/" }
}
