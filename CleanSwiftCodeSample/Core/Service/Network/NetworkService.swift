import Alamofire

class NetworkService {
    
    // Singleton
    static let shared = NetworkService()
    
    func loadHeaders() -> HTTPHeaders {
        
        var headers: HTTPHeaders = [:]
        if let token = UserDataService.sharedObject.getAuthToken() {
            if !token.isEmpty {
                headers["Authorization"] = "Token \(token)"
                headers["X-Platform"] = "ios"
            }
        } else {
            headers["X-Platform"] = "ios"
        }
        return headers
    }
    
    // Private methods
    func checkError(from response: Any) -> (CustomError?, [String: Any]?, [Any]?) {
        
        // Check body
        guard let responseDict = response as? [String: Any] else {
            
            return (CustomError(kind: .emptyBody), nil, nil)
        }
        
        // Check success status
        guard responseDict["success"] as? Bool == false else {
            if let dictData = responseDict["data"] as? [String: Any] {
                
                return (nil, dictData, nil)
            } else if let arrayData = responseDict["data"] as? [Any] {
                
                return (nil, nil, arrayData)
            } else {
                
                return (CustomError(kind: .wrongDataFormat), nil, nil)
            }
        }
        
        // Create server error
        let statusCode = responseDict["statusCode"] as? Int ?? 401
        var messageText = responseDict["message"] as? String
        
        if let dictData = responseDict["data"] as? [String: Any] {
            
            messageText = dictData["message"] as? String
        } else if let arrayData = responseDict["data"] as? [Any], let firstErrorDict = arrayData.last as? [String: Any] {
            
            messageText = firstErrorDict["message"] as? String
        }
        
        let error = CustomError(statusCode: statusCode, message: messageText)
        return (error, nil, nil)
    }
    
    func convertData<T: Decodable>(dataDict: [String: Any], to expectedType: T.Type) -> T? {
        do {
            
            let data = try JSONSerialization.data(withJSONObject: dataDict, options: [])
            return try? JSONDecoder().decode(expectedType, from: data)
        } catch {
            
            debugPrint(error.localizedDescription)
        }
        return nil
    }
}
