//
//  NetworkService + Contacts.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 6/21/19.
//  Copyright © 2019 Petryk Dima. All rights reserved.
//

import Alamofire

extension NetworkService {
    
    func createContact(name: String, number: String, responseSuccess: @escaping() -> Void, responseError: @escaping(CustomError?) -> Void) {
        
        let parameters = [
                "contact": number,
                "name": name
        ] as [String: String]
        
        let request = Alamofire.request(
            APPURL.contactList,
            method: .post,
            parameters: parameters,
            encoding: JSONEncoding.default,
            headers: loadHeaders()
        )
        
        request.responseJSON { response in
            switch response.result {
                
            case .success:
                let (error, _, _) = self.checkError(from: response)
                
                // Check response objects
                if let error = error {
                    if error.statusCode != 666 {
                        responseError(error)
                        return
                    }
                }
                
                responseSuccess()
                return
                
            case .failure:
                let customError = CustomError(statusCode: response.response?.statusCode, message: "Something went wrong")
                responseError(customError)
            }
        }
    }
    
    func getContactsList(searchWord: String?, responseSuccess: @escaping([Contact]) -> Void, responseError: @escaping(CustomError?) -> Void) {
        
        let parameters = ["name_filter": searchWord ?? ""] as [String: String]
        
        let request = Alamofire.request(
            APPURL.contactList,
            method: .get,
            parameters: parameters,
            encoding: URLEncoding.queryString,
            headers: loadHeaders()
        )
        
        request.responseJSON { response in
            switch response.result {
                
            case .success(let value):
                let (error, _, _) = self.checkError(from: response)
                var content: [Contact] = []
                
                // Check response objects
                if let error = error {
                    if error.statusCode != 666 {
                        responseError(error)
                        return
                    }
                }
                
                if let arrayData = value as? [[String: Any]] {
                    
                    for contactDic in arrayData {
                        if let contact = self.convertData(dataDict: contactDic, to: Contact.self) {
                            content.append(contact)
                        }
                    }
                    
                    // Send success
                    responseSuccess(content)
                    return
                }
                
            case .failure:
                let customError = CustomError(statusCode: response.response?.statusCode, message: "Something went wrong")
                responseError(customError)
            }
        }
    }
}
