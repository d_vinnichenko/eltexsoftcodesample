//
//  UserDataService.swift
//  FSF
//
//  Created by Petryk Dima on 4/27/19.
//  Copyright © 2019 Petryk Dima. All rights reserved.
//

import Foundation

let kAuthTokenKey = "authToken"

class UserDataService: NSObject {
    
    // Public props
    static let sharedObject = UserDataService()
    
    // Private props
    private var token: String?
}

// MARK: Token methods
extension UserDataService {
    
    public func saveAuthToken(authToken: String) {
        
        // TODO: Save to keychain
    }
    
    public func getAuthToken() -> String? {
        
        // TODO: Get from keychain
        return nil
    }
    
    public func cleanAuthToken() {
        
        // TODO: Clean keychain value
    }
}

// MARK: User methods
extension UserDataService {
    
    func saveUser(user: User) {
        
        DataBaseManager.shared.saveData(object: user)
    }
    
    func getCurrentUser() -> User? {
        
        return DataBaseManager.shared.loadUserFromDataBase()
    }
    
    func cleanUser() {
        
        DataBaseManager.shared.deleteAllFromDataBase()
    }
    
    func authorizedUser() -> Bool {
        
        if let token = getAuthToken() {
            return !token.isEmpty
        } else {
            return false
        }
    }
}
