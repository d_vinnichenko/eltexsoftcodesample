//
//  DataBaseManager.swift
//  FreeStuffFinder
//
//  Created by Petryk Dima on 6/5/19.
//  Copyright © 2019 EltexSoft. All rights reserved.
//

import Foundation
import RealmSwift

class DataBaseManager {
    
    // Public props
    static let shared = DataBaseManager()
    
    // Private props
    private var realm: Realm
    
    // Lifecycle
    private init() {
        realm = try! Realm()
    }
}

// MARK: Public methods
extension DataBaseManager {
    
    public func saveData(object: Object) {
        try! realm.write {
            realm.add(object, update: .all)
        }
    }
    
    public func saveData(objects: [Object]) {
        try! realm.write {
            realm.add(objects, update: .all)
        }
    }
    
    public func update(_ updateBlock: (() -> Void)) {
        try! realm.write {
            updateBlock()
        }
    }
    
    public func deleteObjectFromDataBase(object: Object) {
        try! realm.write {
            realm.delete(object)
        }
    }
    
    public func deleteAllFromDataBase() {
        try! realm.write {
            realm.deleteAll()
        }
    }
}

// MARK: Special type methods
extension DataBaseManager {
    
    public func loadUserFromDataBase() -> User? {
        let results: Results<User> = realm.objects(User.self)
        return Array(results).first
    }
}
