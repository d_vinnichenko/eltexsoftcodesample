//
//  ContactItem.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 6/21/19.
//  Copyright © 2019 Petryk Dima. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class Contact: Object, Decodable {
    
    // Item props
    @objc private(set) dynamic var id: Int = -1
    @objc dynamic var name = String()
    @objc dynamic var contact = String()
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case contact
    }
    
    // Lifecycle
    required init() {
        super.init()
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.contact = try container.decode(String.self, forKey: .contact)
        super.init()
    }
}
