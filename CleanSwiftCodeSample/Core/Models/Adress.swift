//
//  AdresItem.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 5/16/19.
//  Copyright © 2019 Petryk Dima. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class Adress: Object, Decodable {
    
    // Item props
    @objc private(set) dynamic var id: Int = -1
    @objc dynamic var street = String()
    @objc dynamic var status = Bool()
    
    private enum CodingKeys: String, CodingKey {
        case id
        case street
        case status
    }
    
    // Lifecycle
    required init() {
        super.init()
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.street = try container.decode(String.self, forKey: .street)
        self.status = try container.decode(Bool.self, forKey: .status)
        
        super.init()
    }
}
