//
//  CustomError.swift
//  CleanSwift-ListExample
//
//  Created by Dmitry Vinnichenko on 4/22/19.
//  Copyright © 2019 Dmitry Vinnichenko. All rights reserved.
//

enum ErrorMessage {
    static let emptyBody = "Body is empty."
    static let wrongDataFormat = "Data is wrong format."
    static let other = "Sorry, something went wrong. Please try again in a few minutes."
}

struct CustomError: Error {
    
    enum Kind {
        case custom
        case emptyBody
        case wrongDataFormat
        case other
    }
    
    let statusCode: Int?
    let message: String?
    let kind: Kind?
    
    init(kind: Kind) {
        self.kind = kind
        self.statusCode = 666
        
        switch kind {
        case .emptyBody:
            message = ErrorMessage.emptyBody
            
        case .wrongDataFormat:
            message = ErrorMessage.wrongDataFormat
            
        case .other:
            message = ErrorMessage.other
            
        default:
            message = ErrorMessage.other
        }
    }
    
    init(statusCode: Int?, message: String?) {
        self.kind = .custom
        self.message = message
        self.statusCode = statusCode
    }
}

