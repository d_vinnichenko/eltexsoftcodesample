//
//  User.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 6/12/19.
//  Copyright © 2019 Petryk Dima. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class User: Object, Decodable {
    
    // Item props
    @objc dynamic var token = String()
    
    private enum CodingKeys: String, CodingKey {
        case token
    }
    
    // Lifecycle
    required init() {
        super.init()
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.token = try container.decode(String.self, forKey: .token)
        
        super.init()
    }
}
