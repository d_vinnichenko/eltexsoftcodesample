//
//  AdressItemInfoInteractor.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 6/6/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

// Communication protocol: ViewComtroller -> Interactor
protocol AdressItemInfoBusinessLogic {
    
    func setupAdressInfo(request: AdressInfo.SetupInfo.Request)
    func updateAdress(request: AdressInfo.UpdateAdress.Request)
}

// Protocol for saving data to Interactor
protocol AdressItemInfoDataStore {

    var item: Adress? { get set }
}

class AdressItemInfoInteractor {

    // CleanSwift props
    var presenter: AdressItemInfoPresentationLogic?
    var worker: AdressItemInfoWorker = AdressItemInfoWorker()
    
    // MARK: Private props
    var item: Adress?
}

// MARK: Business Logic
extension AdressItemInfoInteractor: AdressItemInfoBusinessLogic {
    
    func setupAdressInfo(request: AdressInfo.SetupInfo.Request) {
        
        guard let item = item else {
            return
        }
        
        let response = AdressInfo.SetupInfo.Response(item: item)
        presenter?.presentAddresInfo(response: response)
    }
    
    func updateAdress(request: AdressInfo.UpdateAdress.Request) {
        
        if let item = item {
            worker.updateAddres(
                street: request.street,
                status: request.status,
                id: item.id,
                success: { [weak self] in
                    
                    let response = AdressInfo.UpdateAdress.Response(error: nil)
                    self?.presenter?.presentUpdatingAdressStatus(response: response)
                },
                failure: { [weak self] error in
                    
                    let response = AdressInfo.UpdateAdress.Response(error: error)
                    self?.presenter?.presentUpdatingAdressStatus(response: response)
                }
            )
        }
    }
}

// MARK: Data Store
extension AdressItemInfoInteractor: AdressItemInfoDataStore {
}
