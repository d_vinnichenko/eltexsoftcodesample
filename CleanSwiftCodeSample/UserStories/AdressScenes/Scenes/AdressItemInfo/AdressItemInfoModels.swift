//
//  AdressItemInfoModels.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 6/6/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

enum AdressInfo {
    
    enum SetupInfo {
        struct Request {
        }
        struct Response {
            let item: Adress
        }
        struct ViewModel {
            struct DisplayedAddres {
                let street: String
                let status: Bool
            }
            
            var displayedAddres: DisplayedAddres
        }
    }
    
    enum UpdateAdress {
        struct Request {
            var street: String
            var status: Bool
        }
        struct Response {
            var error: Error?
        }
        struct ViewModel {
            var error: Error?
        }
    }
}
