//
//  AdressItemInfoWorker.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 6/6/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

class AdressItemInfoWorker {

    // MARK: - Responses
    internal typealias ResponseSuccess = () -> Void
    internal typealias ResponseFailure = (_ response: CustomError?) -> Void
    
    func updateAddres(street: String, status: Bool, id: Int, success: @escaping ResponseSuccess, failure: @escaping ResponseFailure) {
        
        NetworkService.shared.updateAddress(
            street: street,
            status: status,
            id: id,
            responseSuccess: {
                
                success()
            },
            responseError: { error in
            
                failure(error)
            }
        )
    }
}
