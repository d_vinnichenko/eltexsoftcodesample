//
//  AdressItemInfoPresenter.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 6/6/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

// Communication protocol: Interactor -> Presenter
protocol AdressItemInfoPresentationLogic {
    
    func presentAddresInfo(response: AdressInfo.SetupInfo.Response)
    func presentUpdatingAdressStatus(response: AdressInfo.UpdateAdress.Response)
}

class AdressItemInfoPresenter {
    
    // MARK: CleanSwift props
    weak var viewController: AdressItemInfoDisplayLogic?
}

// MARK: Presentation Logic
extension AdressItemInfoPresenter: AdressItemInfoPresentationLogic {
    
    func presentAddresInfo(response: AdressInfo.SetupInfo.Response) {
        
        let displayedAddres = AdressInfo.SetupInfo.ViewModel.DisplayedAddres(street: response.item.street, status: response.item.status)
        
        let viewModel = AdressInfo.SetupInfo.ViewModel(displayedAddres: displayedAddres)
        viewController?.displayAddresInfo(viewModel: viewModel)
    }
    
    func presentUpdatingAdressStatus(response: AdressInfo.UpdateAdress.Response) {
        
        let viewModel = AdressInfo.UpdateAdress.ViewModel(error: response.error)
        
        if response.error == nil {
            
            viewController?.displayAdressUpdatingSuccess(viewModel: viewModel)
        } else {
            
            viewController?.displayAdressUpdatingError(viewModel: viewModel)
        }
    }
}
