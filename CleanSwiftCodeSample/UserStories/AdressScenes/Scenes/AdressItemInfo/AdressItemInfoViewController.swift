//
//  AdressItemInfoViewController.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 6/6/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

import GrowingTextView
import SVProgressHUD
import UIKit

// Communication protocol: Presenter -> ViewComtroller
protocol AdressItemInfoDisplayLogic: AnyObject {
    
    func displayAddresInfo(viewModel: AdressInfo.SetupInfo.ViewModel)
    func displayAdressUpdatingSuccess(viewModel: AdressInfo.UpdateAdress.ViewModel)
    func displayAdressUpdatingError(viewModel: AdressInfo.UpdateAdress.ViewModel)
}

class AdressItemInfoViewController: UIViewController {
    
    // CleanSwift props
    var interactor: AdressItemInfoBusinessLogic?
    var router: (NSObjectProtocol & AdressItemInfoRoutingLogic & AdressItemInfoDataPassing)?

    // UI Elmenets
    @IBOutlet private weak var streetField: GrowingTextView!
    @IBOutlet private weak var statusSwitch: UISwitch!
    
    // MARK: Lifecycle
    override func awakeFromNib() {
        
        super.awakeFromNib()
        buildScene()
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupItemInfo()
    }
        
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
}

// MARK: Configuration for AdressItemInfoViewController
extension AdressItemInfoViewController {
    
    private func buildScene() {
        
        let interactor = AdressItemInfoInteractor()
        let presenter = AdressItemInfoPresenter()
        let router = AdressItemInfoRouter()
        self.interactor = interactor
        self.router = router
        interactor.presenter = presenter
        presenter.viewController = self
        router.viewController = self
        router.dataStore = interactor
    }
}

// MARK: Private methods
extension AdressItemInfoViewController {
    
    private func setupItemInfo() {
        
        let request = AdressInfo.SetupInfo.Request()
        interactor?.setupAdressInfo(request: request)
    }
    
    // IBActions
    @IBAction private func onUpdate(sender: Any? = nil) {
        
        SVProgressHUD.show()
        
        let request = AdressInfo.UpdateAdress.Request(street: streetField.text, status: statusSwitch.isOn)
        interactor?.updateAdress(request: request)
    }
}

// MARK: Display Logic
extension AdressItemInfoViewController: AdressItemInfoDisplayLogic {
    
    func displayAddresInfo(viewModel: AdressInfo.SetupInfo.ViewModel) {
        
        streetField.text = viewModel.displayedAddres.street
        statusSwitch.isOn = viewModel.displayedAddres.status
    }
    
    func displayAdressUpdatingSuccess(viewModel: AdressInfo.UpdateAdress.ViewModel) {
        
        SVProgressHUD.dismiss()
    }
    
    func displayAdressUpdatingError(viewModel: AdressInfo.UpdateAdress.ViewModel) {
        
        if let error = viewModel.error {
            
            SVProgressHUD.dismiss()
            SVProgressHUD.showError(withStatus: error.localizedDescription)
        }
    }
}
