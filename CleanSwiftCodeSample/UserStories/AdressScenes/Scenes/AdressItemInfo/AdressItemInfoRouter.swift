//
//  AdressItemInfoRouter.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 6/6/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

import UIKit

// Communication protocol: ViewComtroller -> Router
@objc protocol AdressItemInfoRoutingLogic {
}

// Protocol for saving data to Interactor
protocol AdressItemInfoDataPassing {

    var dataStore: AdressItemInfoDataStore? { get }
}

class AdressItemInfoRouter: NSObject {

    // CleanSwift props
    weak var viewController: AdressItemInfoViewController?
    var dataStore: AdressItemInfoDataStore?
}

// MARK: Routing Logic
extension AdressItemInfoRouter: AdressItemInfoRoutingLogic {
}

// MARK: Data Passing
extension AdressItemInfoRouter: AdressItemInfoDataPassing {
}
