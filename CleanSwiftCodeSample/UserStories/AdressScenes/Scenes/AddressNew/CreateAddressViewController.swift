//
//  CreateAddressViewController.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 6/12/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

import GrowingTextView
import SVProgressHUD
import UIKit

// Communication protocol: Presenter -> ViewComtroller
protocol CreateAddressDisplayLogic: AnyObject {
    
    func displayCreatingSuccess(viewModel: CreateAddress.NewAddress.ViewModel)
    func displayCreatingError(viewModel: CreateAddress.NewAddress.ViewModel)
}

class CreateAddressViewController: UIViewController {
    
    // CleanSwift props
    var interactor: CreateAddressBusinessLogic?
    var router: (NSObjectProtocol & CreateAddressRoutingLogic & CreateAddressDataPassing)?

    // UI Elements
    @IBOutlet private weak var streetField: GrowingTextView!
    @IBOutlet private weak var statusSwitch: UISwitch!
    
    // Lifecycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        buildScene()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        view.endEditing(true)
    }
}

// MARK: Configuration for CreateAddressViewController
extension CreateAddressViewController {
    
    private func buildScene() {
        
        let interactor = CreateAddressInteractor()
        let presenter = CreateAddressPresenter()
        let router = CreateAddressRouter()
        self.interactor = interactor
        self.router = router
        interactor.presenter = presenter
        presenter.viewController = self
        router.viewController = self
        router.dataStore = interactor
    }
}

// MARK: Private
extension CreateAddressViewController {
    
    // IBActions
    @IBAction private func backWasTapped(sender: Any? = nil) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func sendWasTapped(sender: Any? = nil) {
        
        view.endEditing(true)
        SVProgressHUD.show()
        
        let request = CreateAddress.NewAddress.Request(street: streetField.text, status: statusSwitch.isOn)
        interactor?.createNewAddress(request: request)
    }
}

// MARK: Display Logic
extension CreateAddressViewController: CreateAddressDisplayLogic {
    
    func displayCreatingSuccess(viewModel: CreateAddress.NewAddress.ViewModel) {
        
        SVProgressHUD.dismiss()
        router?.dismissController()
    }
    
    func displayCreatingError(viewModel: CreateAddress.NewAddress.ViewModel) {
        
        SVProgressHUD.dismiss()
        
        if let error = viewModel.error {
            debugPrint("Manual login error: \(error.localizedDescription)")
        }
    }
}
