//
//  CreateAddressInteractor.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 6/12/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

// Communication protocol: ViewComtroller -> Interactor
protocol CreateAddressBusinessLogic {
    
    func createNewAddress(request: CreateAddress.NewAddress.Request)
}

// Protocol for saving data to Interactor
protocol CreateAddressDataStore {
}

class CreateAddressInteractor {

    // CleanSwift props
    var presenter: CreateAddressPresentationLogic?
    var worker: CreateAddressWorker = CreateAddressWorker()
}

// MARK: Business Logic
extension CreateAddressInteractor: CreateAddressBusinessLogic {
    
    func createNewAddress(request: CreateAddress.NewAddress.Request) {
        
        worker.createNewAddres(
            request: request,
            success: { [weak self] in
                
                let response = CreateAddress.NewAddress.Response(error: nil)
                self?.presenter?.presentCreatingStatus(response: response)
            },
            failure: { [weak self] error in
                
                let response = CreateAddress.NewAddress.Response(error: error)
                self?.presenter?.presentCreatingStatus(response: response)
            }
        )
    }
}

// MARK: Data Store
extension CreateAddressInteractor: CreateAddressDataStore {
}
