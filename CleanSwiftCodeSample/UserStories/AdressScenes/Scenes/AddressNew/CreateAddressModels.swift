//
//  CreateAddressModels.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 6/12/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

enum CreateAddress {
    
    enum NewAddress {
        struct Request {
            var street: String
            var status: Bool
        }
        struct Response {
            var error: Error?
        }
        struct ViewModel {
            var error: Error?
        }
    }
}
