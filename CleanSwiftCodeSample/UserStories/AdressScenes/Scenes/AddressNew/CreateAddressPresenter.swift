//
//  CreateAddressPresenter.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 6/12/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

// Communication protocol: Interactor -> Presenter
protocol CreateAddressPresentationLogic {
    
    func presentCreatingStatus(response: CreateAddress.NewAddress.Response)
}

class CreateAddressPresenter {
    
    // MARK: CleanSwift props b
    weak var viewController: CreateAddressDisplayLogic?
}

// MARK: Presentation Logic
extension CreateAddressPresenter: CreateAddressPresentationLogic {
    
    func presentCreatingStatus(response: CreateAddress.NewAddress.Response) {
        
        let viewModel = CreateAddress.NewAddress.ViewModel(error: response.error)
        
        if response.error == nil {
            
            viewController?.displayCreatingSuccess(viewModel: viewModel)
        } else {
            
            viewController?.displayCreatingError(viewModel: viewModel)
        }
    }
}
