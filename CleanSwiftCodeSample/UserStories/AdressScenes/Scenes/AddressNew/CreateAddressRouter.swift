//
//  CreateAddressRouter.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 6/12/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

import UIKit

// Communication protocol: ViewComtroller -> Router
@objc protocol CreateAddressRoutingLogic {
    
    func dismissController()
}

// Protocol for saving data to Interactor
protocol CreateAddressDataPassing {
}

class CreateAddressRouter: NSObject {

    // CleanSwift props
    weak var viewController: CreateAddressViewController?
    var dataStore: CreateAddressDataStore?
}

// MARK: Routing Logic
extension CreateAddressRouter: CreateAddressRoutingLogic {

    func dismissController() {
        
        viewController?.dismiss(animated: true, completion: nil)
    }
}

// MARK: Data Passing
extension CreateAddressRouter: CreateAddressDataPassing {
}
