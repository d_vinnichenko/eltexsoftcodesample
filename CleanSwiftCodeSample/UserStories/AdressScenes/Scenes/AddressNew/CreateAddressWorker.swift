//
//  CreateAddressWorker.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 6/12/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

class CreateAddressWorker {
    
    // MARK: - Responses
    internal typealias ResponseSuccess = () -> Void
    internal typealias ResponseFailure = (_ response: CustomError?) -> Void
    
    func createNewAddres(request: CreateAddress.NewAddress.Request, success: @escaping ResponseSuccess, failure: @escaping ResponseFailure) {
        
        NetworkService.shared.createAddress(
            street: request.street,
            status: request.status,
            responseSuccess: {
            
                success()
            },
            responseError: { error in
            
                failure(error)
            }
        )
    }
}
