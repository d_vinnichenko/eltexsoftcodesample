//
//  AdressCell.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 6/6/19.
//  Copyright © 2019 Petryk Dima. All rights reserved.
//

import Foundation
import SwipeCellKit
import UIKit

class AdressCell: SwipeTableViewCell {
    
    // UI Elements
    @IBOutlet private weak var streetName: UILabel!
    
    // Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

// MARK: Public
extension AdressCell {
    
    public func setup(address: AdressList.ShowAdressItems.ViewModel.DisplayedAdress) {
        
        self.streetName?.text = address.title
        self.streetName.alpha = address.status ? 1 : 0.2
    }
}
