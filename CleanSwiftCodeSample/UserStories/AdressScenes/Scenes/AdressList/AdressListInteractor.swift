//
//  AdressListInteractor.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 5/16/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

// Communication protocol: ViewComtroller -> Interactor
protocol AdressListBusinessLogic {
    
    func checkAuthorization(request: AdressList.CheckAuthorization.Request)
    func fetchAdressList(request: AdressList.ShowAdressItems.Request)
    func updateAdress(request: AdressList.UpdateAddress.Request)
}

// Protocol for saving data to Interactor
protocol AdressListDataStore {
    
    var items: [Adress] { get }
}

class AdressListInteractor {

    // CleanSwift props
    var presenter: AdressListPresentationLogic?
    var worker: AdressListWorker = AdressListWorker()
    
    // Private props
    var items = [Adress]()
}

// MARK: Business Logic
extension AdressListInteractor: AdressListBusinessLogic {
    
    func checkAuthorization(request: AdressList.CheckAuthorization.Request) {
        
        worker.checkAuthorizationToken(
            success: { [weak self] in
                
                let response = AdressList.CheckAuthorization.Response(isAuthorized: true)
                self?.presenter?.presentAuthorizationStatus(response: response)
            },
            failure: { [weak self] _ in

                let response = AdressList.CheckAuthorization.Response(isAuthorized: false)
                self?.presenter?.presentAuthorizationStatus(response: response)
            }
        )
    }
    
    func fetchAdressList(request: AdressList.ShowAdressItems.Request) {
        
        worker.getAdressList(
            searchWord: request.searchWord,
            success: { [weak self] items in
                // Update data
                self?.items = items
                
                // Send response
                let response = AdressList.ShowAdressItems.Response(items: items, error: nil)
                self?.presenter?.presentAdressList(response: response)
            },
            failure: { [weak self] error in
                // Update data
                self?.items = []
                
                // Send response
                let response = AdressList.ShowAdressItems.Response(items: [], error: error)
                self?.presenter?.presentAdressList(response: response)
            }
        )
    }
    
    func updateAdress(request: AdressList.UpdateAddress.Request) {
        
        worker.updateAdress(
            id: request.id,
            street: request.street,
            status: request.status,
            success: { [weak self] in
                
                // Update
                if let adress = self?.items.first(where: { $0.id == request.id }) {
                    
                    adress.status = request.status
                    
                    let response = AdressList.UpdateAddress.Response(adress: adress, error: nil)
                    self?.presenter?.presentUpdatedAddress(response: response)
                } else {
                    
                    let response = AdressList.UpdateAddress.Response(adress: nil, error: nil)
                    self?.presenter?.presentUpdatedAddress(response: response)
                }
            },
            failure: { [weak self] error in
                
                let response = AdressList.UpdateAddress.Response(adress: nil, error: error)
                self?.presenter?.presentUpdatedAddress(response: response)
            }
        )
    }
}

// MARK: Data Store
extension AdressListInteractor: AdressListDataStore {
}
