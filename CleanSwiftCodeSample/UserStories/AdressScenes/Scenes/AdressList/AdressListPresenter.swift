//
//  AdressListPresenter.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 5/16/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

// Communication protocol: Interactor -> Presenter
protocol AdressListPresentationLogic {
    
    func presentAuthorizationStatus(response: AdressList.CheckAuthorization.Response)
    func presentAdressList(response: AdressList.ShowAdressItems.Response)
    func presentUpdatedAddress(response: AdressList.UpdateAddress.Response)
}

class AdressListPresenter {
    
    // MARK: CleanSwift props
    weak var viewController: AdressListDisplayLogic?
    
    // MARK: Private methods
    private func displayedAdresFormat(from adresses: [Adress]) -> [AdressList.ShowAdressItems.ViewModel.DisplayedAdress] {
        
        var displayedItems = [AdressList.ShowAdressItems.ViewModel.DisplayedAdress]()
        for adress in adresses {
            let displayedAdress = AdressList.ShowAdressItems.ViewModel.DisplayedAdress(id: adress.id, title: adress.street, status: adress.status)
            displayedItems.append(displayedAdress)
        }
        
        return displayedItems
    }
}

// MARK: Presentation Logic
extension AdressListPresenter: AdressListPresentationLogic {
    
    func presentAuthorizationStatus(response: AdressList.CheckAuthorization.Response) {
        
        let viewModel = AdressList.CheckAuthorization.ViewModel()
        
        if response.isAuthorized {
            
            viewController?.displayAuthorizedUserState(viewModel: viewModel)
        } else {
            
            viewController?.displayNotAuthorizedUserState(viewModel: viewModel)
        }
    }
    
    func presentAdressList(response: AdressList.ShowAdressItems.Response) {
        
        let displayedAdresses = displayedAdresFormat(from: response.items)
        let viewModel = AdressList.ShowAdressItems.ViewModel(displayAdresses: displayedAdresses, error: response.error)
        
        if response.error == nil {
            
            viewController?.displayAdresList(viewModel: viewModel)
        } else {
            
            viewController?.displayAdresListError(viewModel: viewModel)
        }
    }
    
    func presentUpdatedAddress(response: AdressList.UpdateAddress.Response) {
        
        guard let adress = response.adress else {
            // TODO: Send custom error
            // viewController?.updateAddressError(viewModel: viewModel)
            return
        }
        
        let displayedAdress = AdressList.ShowAdressItems.ViewModel.DisplayedAdress(id: adress.id, title: adress.street, status: adress.status)
        let viewModel = AdressList.UpdateAddress.ViewModel(displayAdress: displayedAdress, error: response.error)
        
        if response.error == nil {
            
            viewController?.updateAddress(viewModel: viewModel)
        } else {
            
            viewController?.updateAddressError(viewModel: viewModel)
        }
    }
}
