//
//  AdressListModels.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 5/16/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

enum AdressList {
    
    enum CheckAuthorization {
        struct Request {
        }
        struct Response {
            var isAuthorized: Bool
        }
        struct ViewModel {
        }
    }
    
    enum ShowAdressItems {
        struct Request {
            var searchWord: String?
        }
        struct Response {
            var items: [Adress]
            var error: Error?
        }
        struct ViewModel {
            struct DisplayedAdress {
                var id: Int
                var title: String
                var status: Bool
            }
            
            var displayAdresses: [DisplayedAdress]
            var error: Error?
        }
    }
    
    enum UpdateAddress {
        struct Request {
            var id: Int
            var street: String
            var status: Bool
        }
        struct Response {
            var adress: Adress?
            var error: Error?
        }
        struct ViewModel {
            var displayAdress: ShowAdressItems.ViewModel.DisplayedAdress
            var error: Error?
        }
    }
}
