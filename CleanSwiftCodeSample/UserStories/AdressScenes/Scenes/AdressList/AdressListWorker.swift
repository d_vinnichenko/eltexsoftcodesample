//
//  AdressListWorker.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 5/16/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

class AdressListWorker {

    // MARK: - Responses
    internal typealias ResponseUpdatedSuccess = () -> Void
    internal typealias ResponseSuccess = (_ response: [Adress]) -> Void
    internal typealias ResponseFailure = (_ response: CustomError?) -> Void
    
    internal typealias ResponseAuthorizationSuccess = () -> Void

    func getAdressList(searchWord: String?, success: @escaping ResponseSuccess, failure: @escaping ResponseFailure) {
        
        NetworkService.shared.getAdressList(searchWord: searchWord, responseSuccess: success, responseError: failure)
    }
    
    func updateAdress(id: Int, street: String, status: Bool, success: @escaping ResponseUpdatedSuccess, failure: @escaping ResponseFailure) {
        
        NetworkService.shared.updateAddress(street: street, status: status, id: id, responseSuccess: success, responseError: failure)
    }
    
    func checkAuthorizationToken(success: @escaping ResponseAuthorizationSuccess, failure: @escaping ResponseFailure) {
        
        if UserDataService.sharedObject.authorizedUser() {
            success()
        } else {
            failure(nil)
        }
    }
}
