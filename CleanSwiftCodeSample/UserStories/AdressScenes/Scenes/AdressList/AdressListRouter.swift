//
//  AdressListRouter.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 5/16/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

import UIKit

// Communication protocol: ViewComtroller -> Router
@objc protocol AdressListRoutingLogic {
    
    func navigateToLogin()
    func routeToAdressItemInfo(segue: UIStoryboardSegue?)
}

// Protocol for saving data to Interactor
protocol AdressListDataPassing {
    
    var dataStore: AdressListDataStore? { get }
}

class AdressListRouter: NSObject {

    // CleanSwift props
    weak var viewController: AdressListViewController?
    var dataStore: AdressListDataStore?
}

// MARK: Private methods
extension AdressListRouter {
    
    private func passDataToItemInfo(source: AdressListDataStore, destination: inout AdressItemInfoDataStore) {
        
        if let selectedRow = viewController?.tableView.indexPathForSelectedRow?.row {
            destination.item = source.items[selectedRow]
        }
    }
}

// MARK: Routing Logic
extension AdressListRouter: AdressListRoutingLogic {
    
    func navigateToLogin() {
        
        viewController?.performSegue(withIdentifier: "Login", sender: nil)
    }
    
    func routeToAdressItemInfo(segue: UIStoryboardSegue?) {
        if let segue = segue, let adressItemInfoViewController = segue.destination as? AdressItemInfoViewController, var itemInfoDataSource = adressItemInfoViewController.router?.dataStore, let dataStore = dataStore {
            
            passDataToItemInfo(source: dataStore, destination: &itemInfoDataSource)
        }
    }
}

// MARK: Data Passing
extension AdressListRouter: AdressListDataPassing {
}
