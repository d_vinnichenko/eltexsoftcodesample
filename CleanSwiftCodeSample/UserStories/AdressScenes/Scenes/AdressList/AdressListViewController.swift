import SVProgressHUD
import SwipeCellKit
import UIKit

// Communication protocol: Presenter -> ViewComtroller
protocol AdressListDisplayLogic: AnyObject {
    
    func displayAuthorizedUserState(viewModel: AdressList.CheckAuthorization.ViewModel)
    func displayNotAuthorizedUserState(viewModel: AdressList.CheckAuthorization.ViewModel)
    
    func displayAdresList(viewModel: AdressList.ShowAdressItems.ViewModel)
    func displayAdresListError(viewModel: AdressList.ShowAdressItems.ViewModel)
    
    func updateAddress(viewModel: AdressList.UpdateAddress.ViewModel)
    func updateAddressError(viewModel: AdressList.UpdateAddress.ViewModel)
}

class AdressListViewController: UITableViewController {

    // CleanSwift props
    var interactor: AdressListBusinessLogic?
    var router: (NSObjectProtocol & AdressListRoutingLogic & AdressListDataPassing)?

    // Private props
    private var displayedAdress = [AdressList.ShowAdressItems.ViewModel.DisplayedAdress]()
    private let searchController = UISearchController(searchResultsController: nil)
    
    // Lifecycle
    override func awakeFromNib() {
        
        super.awakeFromNib()
        buildScene()
    }
    
    override func viewDidLoad() {

        super.viewDidLoad()
        setupUIElements()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        checkIsUserAuthorized()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
}

// MARK: Configuration for AdressListViewController
extension AdressListViewController {

    private func buildScene() {

        let interactor = AdressListInteractor()
        let presenter = AdressListPresenter()
        let router = AdressListRouter()
        self.interactor = interactor
        self.router = router
        interactor.presenter = presenter
        presenter.viewController = self
        router.viewController = self
        router.dataStore = interactor
    }
}

// MARK: Private methods
extension AdressListViewController {
    
    private func setupUIElements() {
        
        setupSearchBar()
    }
    
    private func setupSearchBar() {
        
        searchController.searchResultsUpdater = self
        searchController.searchBar.placeholder = "Поиск"
        searchController.searchBar.tintColor = UIColor.darkGray
        searchController.searchBar.delegate = self
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    private func fetchAddresList() {
        
        SVProgressHUD.show()
        
        var request = AdressList.ShowAdressItems.Request()
        request.searchWord = searchController.searchBar.text ?? ""
        interactor?.fetchAdressList(request: request)
    }
    
    private func checkIsUserAuthorized() {
        
        let request = AdressList.CheckAuthorization.Request()
        interactor?.checkAuthorization(request: request)
    }
}

// MARK: SwipeTableViewCellDelegate
extension AdressListViewController: SwipeTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        guard orientation == .right else {
            return nil
        }
        
        let title = displayedAdress[indexPath.row].status ? "Inactive" : "Active"
        
        let deleteAction = SwipeAction(style: .default, title: title) { [weak self] _, indexPath in
            if let addres = self?.displayedAdress[indexPath.row] {
                
                SVProgressHUD.show()
                
                let request = AdressList.UpdateAddress.Request(id: addres.id, street: addres.title, status: !addres.status)
                self?.interactor?.updateAdress(request: request)
            }
        }
        
        deleteAction.backgroundColor = UIColor.black
        
        return [deleteAction]
    }
}

// MARK: UISearchBarDelegate
extension AdressListViewController: UISearchResultsUpdating, UISearchBarDelegate {
    
    func updateSearchResults(for searchController: UISearchController) {
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        fetchAddresList()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchController.searchBar.text = ""
        fetchAddresList()
    }
}

// MARK: Display Logic
extension AdressListViewController: AdressListDisplayLogic {
    
    func displayAuthorizedUserState(viewModel: AdressList.CheckAuthorization.ViewModel) {
        
        fetchAddresList()
    }
    
    func displayNotAuthorizedUserState(viewModel: AdressList.CheckAuthorization.ViewModel) {
        
        router?.navigateToLogin()
    }
    
    func displayAdresList(viewModel: AdressList.ShowAdressItems.ViewModel) {
        
        SVProgressHUD.dismiss()
        
        displayedAdress = viewModel.displayAdresses
        tableView.reloadData()
    }
    
    func displayAdresListError(viewModel: AdressList.ShowAdressItems.ViewModel) {
        
        SVProgressHUD.dismiss()
        
        if let error = viewModel.error {
            
            SVProgressHUD.showError(withStatus: error.localizedDescription)
        }
    }

    func updateAddress(viewModel: AdressList.UpdateAddress.ViewModel) {
        
        SVProgressHUD.dismiss()
        
        let updateIndex = displayedAdress.firstIndex { item in
            item.id == viewModel.displayAdress.id
        }
        
        if let index = updateIndex {
            displayedAdress[index].status = viewModel.displayAdress.status
            tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
        }
    }
    
    func updateAddressError(viewModel: AdressList.UpdateAddress.ViewModel) {
        
        SVProgressHUD.dismiss()
        
        if let error = viewModel.error {
            
            SVProgressHUD.showError(withStatus: error.localizedDescription)
        }
    }
}

// MARK: UITableViewDelegate
extension AdressListViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return displayedAdress.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AdressCellIdentifire", for: indexPath) as? AdressCell else {
            return UITableViewCell()
        }
        
        cell.delegate = self
        cell.setup(address: displayedAdress[indexPath.row])
        
        return cell
    }
}
