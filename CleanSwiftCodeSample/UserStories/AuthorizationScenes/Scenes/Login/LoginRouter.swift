//
//  LoginRouter.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 5/17/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

import UIKit

// Communication protocol: ViewComtroller -> Router
@objc protocol LoginRoutingLogic {
    
    func dismissController()
}

// Protocol for saving data to Interactor
protocol LoginDataPassing {
}

class LoginRouter: NSObject {

    // CleanSwift props
    weak var viewController: LoginViewController?
    var dataStore: LoginDataStore?
}

// MARK: Routing Logic
extension LoginRouter: LoginRoutingLogic {
    
    func dismissController() {
        
        viewController?.dismiss(animated: true, completion: nil)
    }
}

// MARK: Data Passing
extension LoginRouter: LoginDataPassing {
}
