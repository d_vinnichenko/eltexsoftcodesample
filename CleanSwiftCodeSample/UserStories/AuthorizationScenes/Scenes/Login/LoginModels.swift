//
//  LoginModels.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 5/17/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

enum Login {
    
    enum ManualLogin {
        struct Request {
            var name: String
            var pass: String
        }
        struct Response {
            var error: Error?
        }
        struct ViewModel {
            var error: Error?
        }
    }
}
