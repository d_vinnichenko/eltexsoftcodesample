//
//  LoginPresenter.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 5/17/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

// Communication protocol: Interactor -> Presenter
protocol LoginPresentationLogic {
    
    func presentStatusAfterLogin(response: Login.ManualLogin.Response)
}

class LoginPresenter {
    
    // MARK: CleanSwift props
    weak var viewController: LoginDisplayLogic?
}

// MARK: Presentation Logic
extension LoginPresenter: LoginPresentationLogic {
    
    func presentStatusAfterLogin(response: Login.ManualLogin.Response) {
        
        let viewModel = Login.ManualLogin.ViewModel(error: response.error)
        
        if response.error == nil {
            
            viewController?.displayLoginSuccess(viewModel: viewModel)
        } else {
            
            viewController?.displayLoginError(viewModel: viewModel)
        }
    }
}
