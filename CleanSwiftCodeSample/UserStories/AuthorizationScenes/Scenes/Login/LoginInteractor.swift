//
//  LoginInteractor.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 5/17/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

// Communication protocol: ViewComtroller -> Interactor
protocol LoginBusinessLogic {
    
    func login(request: Login.ManualLogin.Request)
}

// Protocol for saving data to Interactor
protocol LoginDataStore {
}

class LoginInteractor {

    // CleanSwift props
    var presenter: LoginPresentationLogic?
    var worker: LoginWorker = LoginWorker()
}

// MARK: Business Logic
extension LoginInteractor: LoginBusinessLogic {
    
    func login(request: Login.ManualLogin.Request) {
        
        worker.loginAndSaveToken(
            name: request.name,
            pass: request.pass,
            success: { [weak self] in
                
                let response = Login.ManualLogin.Response(error: nil)
                self?.presenter?.presentStatusAfterLogin(response: response)
            },
            failure: { [weak self] error in
                
                let response = Login.ManualLogin.Response(error: error)
                self?.presenter?.presentStatusAfterLogin(response: response)
            }
        )
    }
}

// MARK: Data Store
extension LoginInteractor: LoginDataStore {
}
