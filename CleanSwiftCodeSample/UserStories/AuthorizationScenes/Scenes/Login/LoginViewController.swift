//
//  LoginViewController.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 5/17/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

import UIKit

// Communication protocol: Presenter -> ViewComtroller
protocol LoginDisplayLogic: AnyObject {
    
    func displayLoginSuccess(viewModel: Login.ManualLogin.ViewModel)
    func displayLoginError(viewModel: Login.ManualLogin.ViewModel)
}

class LoginViewController: UIViewController {
    
    // CleanSwift props
    var interactor: LoginBusinessLogic?
    var router: (NSObjectProtocol & LoginRoutingLogic & LoginDataPassing)?
    
    // UI Elements
    @IBOutlet private weak var nameField: UITextField!
    @IBOutlet private weak var passField: UITextField!

    // Lifecycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        buildScene()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
}

// MARK: Configuration for LoginViewController
extension LoginViewController {
    
    private func buildScene() {
        
        let interactor = LoginInteractor()
        let presenter = LoginPresenter()
        let router = LoginRouter()
        self.interactor = interactor
        self.router = router
        interactor.presenter = presenter
        presenter.viewController = self
        router.viewController = self
        router.dataStore = interactor
    }
}

// MARK: Private
extension LoginViewController {
    
    // IBActions
    @IBAction private func loginWasTapped(sender: Any? = nil) {
        
        if let name = nameField.text, let pass = passField.text {
            let request = Login.ManualLogin.Request(name: name, pass: pass)
            interactor?.login(request: request)
        }
    }
}

// MARK: Display Logic
extension LoginViewController: LoginDisplayLogic {
    
    func displayLoginSuccess(viewModel: Login.ManualLogin.ViewModel) {
        
        router?.dismissController()
    }
    
    func displayLoginError(viewModel: Login.ManualLogin.ViewModel) {
        
        if let error = viewModel.error {
            debugPrint("Manual login error: \(error.localizedDescription)")
        }
    }
}
