//
//  LoginWorker.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 5/17/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

class LoginWorker {

    // MARK: - Responses
    internal typealias ResponseSuccess = () -> Void
    internal typealias ResponseFailure = (_ response: CustomError?) -> Void

    func loginAndSaveToken(name: String, pass: String, success: @escaping ResponseSuccess, failure: @escaping ResponseFailure) {
        
        NetworkService.shared.login(
            name: name,
            password: pass,
            responseSuccess: { token in
                
                debugPrint("Token is \(token)")
                success()
            },
            responseError: { error in
            
                failure(error)
            }
        )
    }
}
