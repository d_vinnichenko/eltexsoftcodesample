//
//  ContactsListInteractor.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 6/21/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

// Communication protocol: ViewComtroller -> Interactor
protocol ContactsListBusinessLogic {
    
    func fetchContactsList(request: ContactsList.ShowContactsList.Request)
}

// Protocol for saving data to Interactor
protocol ContactsListDataStore {
    
    var items: [Contact] { get }
}

class ContactsListInteractor {

    // CleanSwift props
    var presenter: ContactsListPresentationLogic?
    var worker: ContactsListWorker = ContactsListWorker()
    
    // Private props
    var items = [Contact]()
    
    func fetchContactsList(request: ContactsList.ShowContactsList.Request) {
        
        worker.getContactsList(
            searchWord: request.searchWord,
            success: { [weak self] contacts in
                
                // Update data
                self?.items = contacts
                
                // Send response
                let response = ContactsList.ShowContactsList.Response(contacts: contacts, error: nil)
                self?.presenter?.presentContactcList(response: response)
            },
            failure: { [weak self] error in
                
                // Send response
                let response = ContactsList.ShowContactsList.Response(contacts: [], error: error)
                self?.presenter?.presentContactcList(response: response)
            }
        )
    }
}

// MARK: Business Logic
extension ContactsListInteractor: ContactsListBusinessLogic {
}

// MARK: Data Store
extension ContactsListInteractor: ContactsListDataStore {
}
