//
//  ContactsListRouter.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 6/21/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

import UIKit

// Communication protocol: ViewComtroller -> Router
@objc protocol ContactsListRoutingLogic {
}

// Protocol for saving data to Interactor
protocol ContactsListDataPassing {
}

class ContactsListRouter: NSObject {

    // CleanSwift props
    weak var viewController: ContactsListViewController?
    var dataStore: ContactsListDataStore?
}

// MARK: Routing Logic
extension ContactsListRouter: ContactsListRoutingLogic {
}

// MARK: Data Passing
extension ContactsListRouter: ContactsListDataPassing {
}
