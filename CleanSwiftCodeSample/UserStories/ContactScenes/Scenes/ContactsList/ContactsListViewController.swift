//
//  ContactsListViewController.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 6/21/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

import SVProgressHUD
import UIKit

// Communication protocol: Presenter -> ViewComtroller
protocol ContactsListDisplayLogic: AnyObject {
    
    func displayContactsList(viewModel: ContactsList.ShowContactsList.ViewModel)
    func displayContactsListError(viewModel: ContactsList.ShowContactsList.ViewModel)
}

class ContactsListViewController: UITableViewController {
    
    // CleanSwift props
    var interactor: ContactsListBusinessLogic?
    var router: (NSObjectProtocol & ContactsListRoutingLogic & ContactsListDataPassing)?

    // Private props
    private var displayedContacts = [ContactsList.ShowContactsList.ViewModel.DisplayedContact]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        buildScene()
        
        fetchAddresList()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
}

// MARK: Private methods
extension ContactsListViewController {
    
    private func fetchAddresList() {
        
        SVProgressHUD.show()
        
        var request = ContactsList.ShowContactsList.Request()
        request.searchWord = ""
        interactor?.fetchContactsList(request: request)
    }
}

// MARK: Configuration for ContactsListViewController
extension ContactsListViewController {
    
    private func buildScene() {
        
        let interactor = ContactsListInteractor()
        let presenter = ContactsListPresenter()
        let router = ContactsListRouter()
        self.interactor = interactor
        self.router = router
        interactor.presenter = presenter
        presenter.viewController = self
        router.viewController = self
        router.dataStore = interactor
    }
}

// MARK: Display Logic
extension ContactsListViewController: ContactsListDisplayLogic {
    
    func displayContactsList(viewModel: ContactsList.ShowContactsList.ViewModel) {
        
        SVProgressHUD.dismiss()
        
        displayedContacts = viewModel.displayContacts
        tableView.reloadData()
    }
    
    func displayContactsListError(viewModel: ContactsList.ShowContactsList.ViewModel) {
        
        SVProgressHUD.dismiss()

        if let error = viewModel.error {
            
            SVProgressHUD.showError(withStatus: error.localizedDescription)
        }
    }
}

// MARK: UITableViewDelegate
extension ContactsListViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayedContacts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCellIdentifire", for: indexPath) as? ContactCell {
            
//            cell.delegate = self
            cell.setup(contact: displayedContacts[indexPath.row])
            
            return cell
        } else {
            return UITableViewCell()
        }
    }
}
