//
//  ContactsListPresenter.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 6/21/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

// Communication protocol: Interactor -> Presenter
protocol ContactsListPresentationLogic {
    
    func presentContactcList(response: ContactsList.ShowContactsList.Response)
}

class ContactsListPresenter {
    
    // MARK: CleanSwift props
    weak var viewController: ContactsListDisplayLogic?
    
    // MARK: Private methods
    private func displayedContactsFormat(from contacts: [Contact]) -> [ContactsList.ShowContactsList.ViewModel.DisplayedContact] {
        
        var displayContacts = [ContactsList.ShowContactsList.ViewModel.DisplayedContact]()
        for contact in contacts {
            let displayContact = ContactsList.ShowContactsList.ViewModel.DisplayedContact(id: contact.id, name: contact.name, phone: contact.contact)
            displayContacts.append(displayContact)
        }
        
        return displayContacts
    }
}

// MARK: Presentation Logic
extension ContactsListPresenter: ContactsListPresentationLogic {
    
    func presentContactcList(response: ContactsList.ShowContactsList.Response) {
        
        let displayContacts = displayedContactsFormat(from: response.contacts)
        let viewModel = ContactsList.ShowContactsList.ViewModel(displayContacts: displayContacts, error: response.error)
        
        if response.error == nil {
            
            viewController?.displayContactsList(viewModel: viewModel)
        } else {
            
            viewController?.displayContactsListError(viewModel: viewModel)
        }
    }
}
