//
//  ContactsListModels.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 6/21/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

enum ContactsList {
    
    enum ShowContactsList {
        struct Request {
            var searchWord: String?
        }
        struct Response {
            var contacts: [Contact]
            var error: Error?
        }
        struct ViewModel {
            struct DisplayedContact {
                var id: Int
                var name: String
                var phone: String
            }
            
            var displayContacts: [DisplayedContact]
            var error: Error?
        }
    }
}
