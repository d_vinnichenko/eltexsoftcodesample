//
//  ContactsListWorker.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 6/21/19.
//  Copyright (c) 2019 Petryk Dima. All rights reserved.
//
//  This scene is implemented in accordance with the rules of the CleanSwift architecture.
//

// MARK: - Responses
internal typealias ResponseUpdatedSuccess = () -> Void
internal typealias ResponseSuccess = (_ response: [Contact]) -> Void
internal typealias ResponseFailure = (_ response: CustomError?) -> Void

class ContactsListWorker {

    func getContactsList(searchWord: String?, success: @escaping ResponseSuccess, failure: @escaping ResponseFailure) {
        
        NetworkService.shared.getContactsList(searchWord: searchWord, responseSuccess: success, responseError: failure)
    }
}
