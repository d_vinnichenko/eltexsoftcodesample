//
//  ContactCell.swift
//  CleanSwiftCodeSample
//
//  Created by Petryk Dima on 6/26/19.
//  Copyright © 2019 Petryk Dima. All rights reserved.
//

import Foundation
import SwipeCellKit
import UIKit

class ContactCell: SwipeTableViewCell {
    
    // UI Elements
    @IBOutlet private weak var contactName: UILabel!
    
    // Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

// MARK: Public methods
extension ContactCell {
    
    public func setup(contact: ContactsList.ShowContactsList.ViewModel.DisplayedContact) {
        
        self.contactName?.text = contact.name
    }
}
